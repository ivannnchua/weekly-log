|Item|Detail|
|----|-----|
| Name | Ivan Chua Chi Whye |
| Matric Number | 196550 | 
| Week | 12 |
| Date | 10/1/2022 - 14/1/2022 |
| Subsystem | Propulsion and Power |
| Group | 2 |

### Agenda and Assigned Tasks
For the propulsion subsystem, we continued to work on our report. We are currently working on how to compact the report and also make some adjustments on the visualised data (the graph)


### What Went Wrong, What Could be Improved and Future Plans
For the following week, we would be having the airship fly (on 21st of January), we would assist the other subsystems as much as possible (especially the structures team as our motors would be under their care).
