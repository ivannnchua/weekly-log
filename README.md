Name: Ivan Chua Chi Whye
<br>
Matric Number: 196550
<br>
AEROSPACE DESIGN PROJECT (EAS4947)
<br>
Group 2 / Propulsion and Power Subsystem
<br>

## 1. Abstract
This report covers the weekly logs on what has been done for the aerospace design project for the entire semester. This report will primarily cover on propulsion and power as I have been assigned to this subsystem. However, other subsystems will also be discussed in this report. A design of an airship with sprayers will be discussed in this report in which this airship will be used for agricultural purposes in which the sprayer will be filled. 

## [2. Introduction]()
 

## 3.Link to the Weekly Logs
| Week | Link |
| ------ | ------ |
| 1 | *unavailable* |
| 2  | [Week 2 Markdown File](https://gitlab.com/ivannnchua/weekly-log/-/blob/main/Week_2.md) |
| 3  |  |
| 4 |  |
| 5 |  |
| 6 |  |
| 7 |  |
| 8 |  | 
| 9 |  |  
| 10 |  | 
| 11 |  | 
| 12 |  | 
| 13 |  | 
| 14 |  | 
