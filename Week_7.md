|Item|Detail|
|----|-----|
| Name | Ivan Chua Chi Whye |
| Matric Number | 196550 | 
| Week | 7 |
| Date | 29/11/2021 - 3/12/2021 |
| Subsystem | Propulsion and Power |
| Group | 2 |

### Agenda and Assigned Tasks
|Subsystem|Group|
|----|-----|
| For this week, we did not go to the propulsion lab to carry out any experiments as we have not received the new components by the supplier. However, we had a meeting with Dr. Ezanee on Wednesday, 1st December 2021 to discuss the issues that we faced the previous week. Some of the issues and confusion include, 1) How significant is vibration to the performance of the airship, 2) What are we going to do with the values of current, 3) How are we going to land the airship | No assigned responsibilty for this week. |



### Decisions Made Individually/In a Team and How Did We Decide
|Subsystem|Group|
|----|-----|
| No decisions to be made this week. However, we did decide to finalise some results and set thresholds of current for the working performance of the battery (theoretically) |  |


### Why Did We Make THAT Decision
|Subsystem|Group|
|----|-----|
| Setting the maximum threshold for the battery and motor performance will prevent the motors to be overworked. |  |


### What did I Learn
Calculations of different performance parameters.

### What Went Wrong, What Could be Improved and Future Plans
We would plan to carry out the thrust test ideally when we receive the components from the supplier. 
