|Item|Detail|
|----|-----|
| Name | Ivan Chua Chi Whye |
| Matric Number | 196550 | 
| Week |  |
| Date |  |
| Subsystem | Propulsion and Power |
| Group | 2 |

### Agenda and Assigned Tasks
|Subsystem|Group|
|----|-----|
| Analysis of the thrust test data was done during this week and not many activities were done in the lab to avoid as much contact as possible due to the previous positive case in the lab. Propulsion report was done by our individual parts. |  |

