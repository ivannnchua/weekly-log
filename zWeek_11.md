|Item|Detail|
|----|-----|
| Name | Ivan Chua Chi Whye |
| Matric Number | 196550 | 
| Week | 11 |
| Date | 3/1/2021 - 7/1/2021 |
| Subsystem | Propulsion and Power |
| Group | 2 |

### Agenda and Assigned Tasks
|Subsystem|Group|
|----|-----|
| Data analysis for the thrust test from the previous week (Week 9 and Week 10) as well as continuing to write the report for the IDP | - |



