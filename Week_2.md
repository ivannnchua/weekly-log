|Item|Detail|
|----|-----|
| Name | Ivan Chua Chi Whye |
| Matric Number | 196550 | 
| Week | 2 |
| Date | 25/10/2021 - 29/10/2021 |
| Subsystem | Propulsion and Power |
| Group | 2 |

### Agenda and Assigned Tasks
During the first meeting with our subsystems, we were divided into groups and had discussions via Discord to roughly sketch out our plan for the semester by drafting a [Gantt chart](https://docs.google.com/spreadsheets/d/1Up794u-fh7eLmh39m1LQePHybpP-Hctlfa6DtNqmC68/edit?usp=sharing) for each subsystem. 

### Decisions Made Individually/In a Team and How Did We Decide
At this stage, we did not have any individual decisions that needed to be made. However, we discussed collectively on how the [Gantt chart](https://docs.google.com/spreadsheets/d/1Up794u-fh7eLmh39m1LQePHybpP-Hctlfa6DtNqmC68/edit?usp=sharing) would look like and how we would expect to work throughout the semester - what are we going to do, when are we going to do, how are we going to execute those plans. 

### Why Did We Make THAT Decision
So far, we only decided to create a [Gantt chart](https://docs.google.com/spreadsheets/d/1Up794u-fh7eLmh39m1LQePHybpP-Hctlfa6DtNqmC68/edit?usp=sharing) that looks realistic as we were not sure of what we could do at that time. We as a whole subsystem have never checked the inventory to see what works and what does not so we just drafted the [Gantt chart](https://docs.google.com/spreadsheets/d/1Up794u-fh7eLmh39m1LQePHybpP-Hctlfa6DtNqmC68/edit?usp=sharing), ensuring there are tolerances between weeks, in case any difficulties arise. 

### What did I Learn
Learnt how to organise [Gantt charts](https://docs.google.com/spreadsheets/d/1Up794u-fh7eLmh39m1LQePHybpP-Hctlfa6DtNqmC68/edit?usp=sharing) in an orderly manner. 


### What Went Wrong, What Could be Improved and Future Plans
Nothing went wrong this week. However, the [Gantt chart](https://docs.google.com/spreadsheets/d/1Up794u-fh7eLmh39m1LQePHybpP-Hctlfa6DtNqmC68/edit?usp=sharing) is not a finalised version and is just a draft. Therefore, some amendments might occur and some agenda might come earlier or happen later. 
<br>
For the following week, we plan to go to the propulsion lab to see for ourselves the equipments available for tests. We plan to do our thrust test in [Week 3](https://gitlab.com/ivannnchua/weekly-log/-/blob/main/Week_3.md).
