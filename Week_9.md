|Item|Detail|
|----|-----|
| Name | Ivan Chua Chi Whye |
| Matric Number | 196550 | 
| Week | 9 |
| Date | 20/12/2021 - 24/12/2021 |
| Subsystem | Propulsion and Power |
| Group | 2 |

No activities were held this week. Postponed classes due to the flood and positive covid cases in the lab.

### Future Plans
The Propulsion subsystem have already begun our report writing. We will be working on it remotely.
