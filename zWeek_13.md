|Item|Detail|
|----|-----|
| Name | Ivan Chua Chi Whye |
| Matric Number | 196550 | 
| Week | 13 |
| Date | 17/1/2021 - 21/1/2021 |
| Subsystem | Propulsion and Power |
| Group | 2 |

### Agenda and Assigned Tasks
Physical meeting with Dr. Salah was carried out outside H2.1 lab to summarise what needs to be done to fly the airship next week on week 14 (Friday).
We are expected to have a checklist on what needs to be checked from each subsystem so that the following week would be a smooth sailing one. 
There will be a scheduled meeting on Saturday of week 13 (22/1/2022) to discuss about the checklist that needs to be gone through on week 14.
