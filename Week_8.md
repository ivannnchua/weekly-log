|Item|Detail|
|----|-----|
| Name | Ivan Chua Chi Whye |
| Matric Number | 196550 | 
| Week | 8 |
| Date | 13/12/2021 - 17/12/2021 |
| Subsystem | Propulsion and Power |
| Group | 2 |

### Agenda and Assigned Tasks
|Subsystem|Group|
|----|-----|
| We waited for the new motors to arrive on Tuesday. However, we were told that it was not necessary to run the thrust test again for the new motors. | This week, I was not assigned for any tasks. However, we did discuss a little to update after the mid semester break. |


### Decisions Made Individually/In a Team and How Did We Decide
|Subsystem|Group|
|----|-----|
| We decided to not run the motors to avodid burning them while testing. Initially, we wanted to run the test but we decided not to. Reason will be discussed in the next section. |  |


### Why Did We Make THAT Decision
|Subsystem|Group|
|----|-----|
| We decdied not to run the thrust test because we did not want to burn the motors as we only have the right and sufficient amount of motors and ESC for the real airship. We wouldn't want to damage any one of it before the flight. |  |


### What Went Wrong, What Could be Improved and Future Plans
For the following week, after our PIGS on Saturday with the other subsystems and Dr. Salah, we have decided to also carry out the thrust test in order to know the performance of the motors so that we can estimate what needs to be done with the motors and if it is powerful enough to drive the airship. 
