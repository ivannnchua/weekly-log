|Item|Detail|
|----|-----|
| Name | Ivan Chua Chi Whye |
| Matric Number | 196550 | 
| Week | 5 |
| Date | 15/11/2021 - 19/11/2021 |
| Subsystem | Propulsion and Power |
| Group | 2 |

### Agenda and Assigned Tasks
|Subsystem|Group|
|----|-----|
| On Wednesday (17/11/2021) the propulsion team went to the propulsion lab again to carry out thrust test with the old motors with similar specifications with the newly ordered motors which is the [Tarot 4114 320KV](http://www.tarotrc.com/Product/Detail.aspx?Lang=en&Id=7caf622a-6119-4aaf-90e2-ed846acfb13f). The condition of the motors is shown below. | I was assigned to be the checker for week 5 in which I will be responsible for checking the reports and making sure that all of my groupmates are on track. |
|Dr. Ezanee gave a brief explanantion on how we should be able to find the thrust that is required from the equilibrium of forces of the airship as shown in this file - [Thrust Plot - 17/11/2021](https://gitlab.com/putraspace/idp-2021-group/propulsion-and-power/uploads/b97898bac28243ae46bc53ce70aac771/NOTE_1601121.pdf)



|Motor Number|Condition|Additional Remarks|
|---|---|---|
| 1 | Non-functional | Cannot spin properly|
| 2 | Functioning | Thrust = 1kg, Current = 4A (not maximum throttle)|
| 3 | Functioning | Thrust = 0.48kg, Current = 9A (a lot of vibration)|
| 4 | Burnt | Burnt previously because of the use of unsuitable propeller size |
* all motors tested with a 17inch propeller

### Decisions Made Individually/In a Team and How Did We Decide
|Subsystem|Group|
|----|-----|
| The propulsion subsystem have decided to test all 4 motors from the previous airship. This is due to the same reason with last week in which we have to familiarise ourselves with the working system of the software in order to get consistent results. |  |


### Why Did We Make THAT Decision
|Subsystem|Group|
|----|-----|
| To ease our job when the new motors arrive, we would be able to implement the motors straight away onto the test bench and run the test right away with minimal issues. |  |


### What did I Learn
I learnt how to find the desired drag and thrust from the equilibrium of forces from the airship in which Dr. Ezanee had explained this week.

### What Went Wrong, What Could be Improved and Future Plans
An improvement that can be made is that we would find the right amount of throttle percentage that the motor can withstand because we actually do not know the maximum performance of the motor when we installed the 17inch propeller. 
