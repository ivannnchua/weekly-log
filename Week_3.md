|Item|Detail|
|----|-----|
| Name | Ivan Chua Chi Whye |
| Matric Number | 196550 | 
| Week | 3 |
| Date | 1/11/2021 - 5/11/2021 |
| Subsystem | Propulsion and Power |
| Group | 2 |

### Agenda and Assigned Tasks
|Subsystem|Group|
|----|-----|
| On Wednesday (3/11/2021), four of us - Tareena, Abdul Rahman Lee, Izzat Ikhwan and I went to the propulsion lab to learn how to use the thrust test experimental setup. Dr. Ezanee was present to teach us how to operate the setup. | Weekly group update during IDP Class on Friday (5/11/2021) and assigned Hanis as coordinator, Tareena as recorder, Asfeena as checker and Ivan as process monitor. |
<img src = "https://gitlab.com/ivannnchua/weekly-log/uploads/2aac730a2f364cc869993bfe0c38148b/WhatsApp_Image_2021-11-03_at_5.24.04_PM.jpeg" alt="Thrust test" width="200px" height="auto">


### Decisions Made Individually/In a Team and How Did We Decide
|Subsystem|Group|
|----|-----|
| During the propulsion thrust test, we had difficulties in deciding the diameter of the propeller used to provide the thrust to the airship. Therefore, we tried 17 inch and 23 inch propellers to a sample motor in the propulsion lab but unfortunately, the motor couldn't stand 23 inch propeller and it burnt. Therefore, we have decided not to deal with the 23 inch propeller anymore as it will cause damage at only about 50% of throttle. | Decisions that we made in the mixed group include choosing the roles for each person. However, it wasn't a difficult task as we recommended to have a duty roster in which we would rotate our turns every week. The Excel sheet can be seen here. [Duty Roster Excel](https://docs.google.com/spreadsheets/d/11jA2B6f_ckZ-tsQE98Xk9M-lD-hem7uPyygdEp6gmRc/edit#gid=711576105) |


### Why Did We Make THAT Decision
|Subsystem|Group|
|----|-----|
| As mentioned earlier, when using the 23 inch propeller, the motor burnt when the throttle have not even reached 50% of the overall maximum throttle it could achieve. Therefore, we have decided - due to safety reasons, not to use the 23 inch propeller, even in the future. Unless the specification sheet from the manufacturer states that it is possible. | Having a duty roster in a rotation basis is a good way to ensure that every team member would have the chance to be in charge of something. |


### What did I Learn
During the whole week, I learnt how to operate RCBenchmark - the software and test kit used to carry out the thrust test. Week 3 was basically an introduction week on how to operate the test apparatus. 

### What Went Wrong, What Could be Improved and Future Plans
Obviously, the burnt motor went wrong. We could improve that by actually checking the specification sheet provided by the manufacturer in which they would give a range of dimensions for the recommended sized of propellers that the motor can take. 
For the following week, we have plans to run the thrust test again with motors readily available in the propulsion lab which have similar specifications as the motor that we have ordered (T-Motor MN5212 KV340).
