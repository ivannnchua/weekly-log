|Item|Detail|
|----|-----|
| Name | Ivan Chua Chi Whye |
| Matric Number | 196550 | 
| Week | 6 |
| Date | 22/11/2021 - 26/11/2021 |
| Subsystem | Propulsion and Power |
| Group | 2 |

### Agenda and Assigned Tasks
|Subsystem|Group|
|----|-----|
| On Wednesday, carry out thrust test and this time thrust at full throttle. The obtained value of thrust was about 1200gf on a 15.8V battery pack.  | In my individual group, I ensured that everyone was clear on what they were doing and had each of the subsystems update on what they have already done the past week. |


### Decisions Made Individually/In a Team and How Did We Decide
|Subsystem|Group|
|----|-----|
| I have decided that we are to have a meeting before the Friday class and short meeting after the Friday class. The whole team decided that we carry out the meeting so that every member are on the right track and are not lost. It was not a tough decision as it benefits all of us. | - |
| We decided to still carry out the thrust test although we do not have the real motors yet. This was done so that we were able to know and gauge about how much thrust we were able to obtain when we receive our new motors and ESC. Besides that, continuous running of the thrust test would enable us to familiarise ourselves with the system so that when the real thing comes, we could carry out the test smoothly without any difficulties. | - | 


### Why Did We Make THAT Decision
|Subsystem|Group|
|----|-----|
| Ensure that each member of the subsystem is on track and are not left behind. | - |
| Ensure a smooth thrust test in the future (we made some mistakes for the past few weeks and all of those mistakes were taken into consideration and remembered so that we will NOT repeat the same mistakes in the future.)| - |

### What did I Learn
I learnt to be more organised this past few weeks and realised that having teamwork and also having each team member to be clear with the subject is a very important matter. Short meetings to reiterate the discussed matter can be done to ensure everyone is on track.


### What Went Wrong, What Could be Improved and Future Plans
As mentioned earlier, a mistake was made during the test in which installation of the propeller leading edge was wrong, leading to a bad reading on the meter and also high vibration. We managed to solve the problem. Besides that, it is also important to always check the connections of wires because one of the wires came loose and the thrust started to fluctuate when reached a certain value (due to strong vibration, the cable was disconnecting and connecting a little bit periodically.) In the future, we will ensure that these mistakes will NOT be repeated. 
