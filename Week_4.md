|Item|Detail|
|----|-----|
| Name | Ivan Chua Chi Whye |
| Matric Number | 196550 | 
| Week | 4 |
| Date | 8/11/2021 - 12/11/2021 |
| Subsystem | Propulsion and Power |
| Group | 2 |

### Agenda and Assigned Tasks
|Subsystem|Group|
|----|-----|
| On Wednesday (10/11/2021) the propulsion team went to them lab to test the functionality of the motors. Reason for testing the motors was to actually get used with the system of the software. We were taught on how to carry out the test and how to record the data on the software (RCBenchmark). It will be a routine work, therefore it is important for us to know the steps and follow them closely to obtain the best results. | I was assigned to be the process monitor this week. I was responsible in ensuring that the flow of our meeting went smooth. Every agenda was achieved. Initiated discussions on each subsystem and I have requested each subsystem to update on their progress for the week. |
| Reading on the Fundamentals of Airship Design Volume 2 textbook. |  | 


### Decisions Made Individually/In a Team and How Did We Decide
|Subsystem|Group|
|----|-----|
| We decided to continue testing the motors. This was an easy decision after discussing virtually as we have already set the timing and agenda the previous week. | Decision making in order to update each subsystems' progress so that we are all on the same track. |


### Why Did We Make THAT Decision
|Subsystem|Group|
|----|-----|
| In order to familiarise ourselves with the system and the test procedures, we have decided that we test the old motors so that when the new ones arrive, we would have a rough idea on how we would actually be able to operate the motors and also to avoid damaging the motor. |  |


### What did I Learn
I learnt how to operate the RCBenchmark software properly and accordingly. Knowing how to setup the test setup was also a new knowledge. 


### What Went Wrong, What Could be Improved and Future Plans
The propulsion subsystem was unsure on the parameters needed to drive the propeller and also the amount of thrust needed to move the airship. Therefore, for the following week, we aim to find the performance of the motors. 
